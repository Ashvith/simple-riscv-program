{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  outputs = inputs@{ flake-parts, ... }:
  flake-parts.lib.mkFlake { inherit inputs; } {
    systems = [
      "x86_64-linux" "x86_64-darwin"
      "aarch64-linux" "aarch64-darwin"
    ];
    perSystem = { config, self', inputs', pkgs, system, ... }:
    {
      _module.args.pkgs = import inputs.nixpkgs {
        inherit system;
        crossSystem.config = "riscv64-unknown-linux-gnu";
      };
      devShells.default = with pkgs; mkShell {
        buildInputs = [
          binutils
          gcc
        ];
      }; 
    };
  };
}
