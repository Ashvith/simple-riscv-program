# Simple RISC-V program

This is a simple RISC-V program from [Low Level Learning](https://youtu.be/GWiAQs4-UQ0). To use it, you need to have the RISC-V GNU toolchain and QEMU installed. For the sake of convenience, I've also provided a simple dev shell using flake-parts, which can by accessed by the `nix develop --ignore-environment` command (requires Nix to be installed and experimental features enabled - in this case, Nix flakes).

## Instructions
1. Run `make` to build the program.
2. Use `qemu-riscv64 hello` to execute the program.
